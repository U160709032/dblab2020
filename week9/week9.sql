CREATE OR REPLACE VIEW usa_customers AS 
SELECT CustomerID, CustomerName, ContactName
FROM customers 
WHERE Country = "USA";

SELECT * 
FROM usa_customers JOIN orders ON usa_customers.CustomerID = orders.CustomerID;

CREATE VIEW products_below_awg_price AS
SELECT ProductID, ProductName, Price
FROM Products
WHERE Price<(SELECT avg(Price) FROM Products);
